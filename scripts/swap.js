

async function main() {

    routeraddress = '0xfE006d4dc2406cE868991c904119b13E5116C504';
    pair00addr = '0x42fc1ddFa10f3320A1a63Eb956ea4A9155e7994e';//type 0 pair
    pair10addr = '0x6b545a435FE9F88980Ea17c8D31b30A2ADd3A9Ea';//type 1 pair
    pair20addr = '0x94170e8F91EEcd75d1d86f4D454d25Fd155C9aFD';//type 2 pair
    owner = '0x698c514c49C3E1C4285fc87674De84cd56A72646';

    const Router = await ethers.getContractFactory('LSSVMRouter');//geeting router contract
    const router = await Router.attach(routeraddress);



    deadline = Date.now() + 10000;//epoch time

    let swaptxn0 = await router.swapNFTsForToken( //swap on type0 pair
        [[pair00addr, [109]]],//109 is nftid which must be with caller
        hre.ethers.utils.parseEther('0.0000005'), //minimum amount
        owner,
        deadline
    )

    await swaptxn0.wait();
    console.log('Sucessfully Swapped NFt for eth using type 0 pool ', swaptxn0.hash);


    let swaptxn1 = await router.swapETHForAnyNFTs( //swap on type 1 pool
        [[pair10addr, 1]], //anynft can be swapped
        owner,
        owner,
        deadline,
        {
            value: ethers.utils.parseEther("0.001")//initail eth send to pool
        }
    )

    await swaptxn1.wait();
    console.log('Sucessfully Swapped eth for nft using type 1 pool ', swaptxn1.hash);


    let swaptxn2 = await router.swapETHForAnyNFTs( //swap on pool 2
        [[pair20addr, 1]],
        owner,
        owner,
        deadline,
        {
            value: ethers.utils.parseEther("0.001")//initail eth send to pool
        }
    )

    await swaptxn2.wait();
    console.log('Sucessfully Swapped eth for nft using type 2 pool', swaptxn2.hash);


    let swaptxn3 = await router.swapNFTsForToken(
        [[pair20addr, [101]]], // nft id 101 must be present with caller
        hre.ethers.utils.parseEther('0.00000005'),
        owner,
        deadline
    )

    await swaptxn3.wait();
    console.log('Sucessfully Swapped nft for eth using type 2 pool', swaptxn3.hash);





}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
