const hre = require("hardhat");
const { ethers } = require("hardhat");

async function main() {




    // return;
    let LSSVMPairEnumerableETHAddress, LSSVMPairMissingEnumerableETHAddress,
        lSSVMPairEnumerableERC20Address, LSSVMPairMissingEnumerableERC20Address,
        LSSVMPairFactoryAddress, LSSVMRouterAddress, exponentialCurveAddress,
        linearCurveAddress;


    //settings
    let PROTOCOL_FEE_MULTIPLIER = 0;
    let PROTOCOL_FEE_RECIPIENT = "0x698c514c49C3E1C4285fc87674De84cd56A72646";
    let ADMIN = "0x698c514c49C3E1C4285fc87674De84cd56A72646";


    const LSSVMPairEnumerableETH = await ethers.getContractFactory("LSSVMPairEnumerableETH");
    const lSSVMPairEnumerableETH = await LSSVMPairEnumerableETH.deploy();
    await lSSVMPairEnumerableETH.deployed();

    LSSVMPairEnumerableETHAddress = lSSVMPairEnumerableETH.address
    console.log("lSSVMPairEnumerableETH deployed at Address: ", lSSVMPairEnumerableETH.address)



    const LSSVMPairMissingEnumerableETH = await ethers.getContractFactory("LSSVMPairMissingEnumerableETH");
    const lSSVMPairMissingEnumerableETH = await LSSVMPairMissingEnumerableETH.deploy();
    await lSSVMPairMissingEnumerableETH.deployed();

    LSSVMPairMissingEnumerableETHAddress = lSSVMPairMissingEnumerableETH.address;
    console.log("LSSVMPairMissingEnumerableETH deployed at Address: ", lSSVMPairMissingEnumerableETH.address)



    const LSSVMPairEnumerableERC20 = await ethers.getContractFactory("LSSVMPairEnumerableERC20");
    const lSSVMPairEnumerableERC20 = await LSSVMPairEnumerableERC20.deploy();
    await lSSVMPairEnumerableERC20.deployed();

    lSSVMPairEnumerableERC20Address = lSSVMPairEnumerableERC20.address;
    console.log("LSSVMPairEnumerableERC20 deployed at Address: ", lSSVMPairEnumerableERC20.address)



    const LSSVMPairMissingEnumerableERC20 = await ethers.getContractFactory("LSSVMPairMissingEnumerableERC20");
    const lSSVMPairMissingEnumerableERC20 = await LSSVMPairMissingEnumerableERC20.deploy();
    await lSSVMPairMissingEnumerableERC20.deployed();

    LSSVMPairMissingEnumerableERC20Address = lSSVMPairMissingEnumerableERC20.address;
    console.log("LSSVMPairMissingEnumerableERC20 deployed at Address: ", lSSVMPairMissingEnumerableERC20.address)


    const LSSVMPairFactory = await ethers.getContractFactory("LSSVMPairFactory");
    const lSSVMPairFactory = await LSSVMPairFactory.deploy(LSSVMPairEnumerableETHAddress, LSSVMPairMissingEnumerableETHAddress,
        lSSVMPairEnumerableERC20Address, LSSVMPairMissingEnumerableERC20Address, PROTOCOL_FEE_RECIPIENT, PROTOCOL_FEE_MULTIPLIER);
    await lSSVMPairFactory.deployed();

    LSSVMPairFactoryAddress = lSSVMPairFactory.address;
    console.log("LSSVMPairFactory deployed at Address: ", lSSVMPairFactory.address)
    console.log(`constructor args: "${LSSVMPairEnumerableETHAddress}" "${LSSVMPairMissingEnumerableETHAddress}"
        "${lSSVMPairEnumerableERC20Address}" "${LSSVMPairMissingEnumerableERC20Address}" "${PROTOCOL_FEE_RECIPIENT}" "${PROTOCOL_FEE_MULTIPLIER}"`)



    const LSSVMRouter = await ethers.getContractFactory("LSSVMRouter");
    const lSSVMRouter = await LSSVMRouter.deploy(LSSVMPairFactoryAddress);
    await lSSVMRouter.deployed();

    LSSVMRouterAddress = lSSVMRouter.address;
    console.log("LSSVMRouter deployed at Address: ", lSSVMRouter.address)



    const LSSVMRouter1 = await ethers.getContractAt("LSSVMPairFactory", LSSVMPairFactoryAddress);
    const setAllowed = await LSSVMRouter1.setRouterAllowed(LSSVMRouterAddress, true);
    await setAllowed.wait();

    console.log("LSSVMRouter has been whitelisted in Factory: ", setAllowed.hash);



    const ExponentialCurve = await ethers.getContractFactory("ExponentialCurve");
    const exponentialCurve = await ExponentialCurve.deploy();
    await exponentialCurve.deployed();

    exponentialCurveAddress = exponentialCurve.address;
    console.log("ExponentialCurve deployed at Address: ", exponentialCurve.address)



    const LinearCurve = await ethers.getContractFactory("LinearCurve");
    const linearCurve = await LinearCurve.deploy();
    await linearCurve.deployed();

    linearCurveAddress = linearCurve.address;
    console.log("LinearCurve deployed at Address: ", linearCurve.address)


    const LSSVMRouter2 = await ethers.getContractAt("LSSVMPairFactory", LSSVMPairFactoryAddress);

    const setAllowedExponential = await LSSVMRouter2.setBondingCurveAllowed(exponentialCurveAddress, true);
    await setAllowedExponential.wait();

    const setAllowedLinear = await LSSVMRouter2.setBondingCurveAllowed(linearCurveAddress, true);
    await setAllowedLinear.wait();

    console.log("ExponentialCurve whitelisted at: ", setAllowedExponential.hash);
    console.log("LinearCurve whitelisted at: ", setAllowedLinear.hash);


    const LSSVMRouter3 = await ethers.getContractAt("LSSVMPairFactory", LSSVMPairFactoryAddress);

    const transferOwnership = await LSSVMRouter3.transferOwnership(ADMIN);
    await transferOwnership.wait();
    console.log("Transferred factory ownership.\nNew Owner: ", ADMIN, "\nHash: ", transferOwnership.hash);






}


main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
