const { ethers } = require("hardhat");






async function main() {

    //previously deployed addresses 
    nftaddress = '0xE63b51649A1778A9c1666D24b905645838C514ac';
    //enumnftaddress = '';
    owner = '0x698c514c49C3E1C4285fc87674De84cd56A72646';
    factoryaddress = '0x673901f0a0b0b10036477D14c0aD279fB0147bB1';
    linearcurveaddress = '0x0FBeCDb9Eafa5561A72fBd4f150dea987b3285e4';
    //exponentialcurveaddress = '';




    //ensure nfts are minted to caller account






    const Factory = await ethers.getContractFactory('LSSVMPairFactory');//factory contract
    const factory = await Factory.attach(factoryaddress);


    let Pair00 = await factory.createPairETH(
        nftaddress,//nft  /// change nftaddress to enumerable for enumerablepair operations
        linearcurveaddress,//curve to be used
        owner, //who is going to recieve the proceeds of sales
        0,//type 0 pool
        hre.ethers.utils.parseEther('0.00095'), //delta value (bignumber)
        hre.ethers.utils.parseEther('0'), //fee
        hre.ethers.utils.parseEther('0.0001'), //price of each NFT (spot price)
        [100, 101, 102], //NFT ids you want to transfer

        {
            value: ethers.utils.parseEther("0.001")//initail eth send to pool
        }
    )


    let txn = await Pair00.wait();
    console.log('Pool of type 0 created');
    console.log('Pair00 Hash', Pair00.hash);
    console.log('Pair00 BlockNumber', txn.blockNumber);
    console.log('Pair00 Logs', txn.logs);
    console.log('Pair00 Events', txn.events);
    var receipt = await hre.ethers.provider.getTransactionReceipt('0x756facdcf284b9053ebe1d899e990fc42baa01db05845501ddc457390cd8b0d5');
    console.log(receipt);





    let Pair10 = await factory.createPairETH(
        nftaddress,//nft
        linearcurveaddress,//curve to be used
        owner, //who is going to recieve the proceeds of sales
        1,//type 1 pool
        hre.ethers.utils.parseEther('0.000095'), //delta value (bignumber)
        hre.ethers.utils.parseEther('0'), //fee
        hre.ethers.utils.parseEther('0.0000123'), //price of each NFT (spot price)
        [103, 104], //NFT ids you want to transfer

    )

    let txn2 = await Pair10.wait();
    console.log('Pool of type 0 created');
    console.log('Pair10 Hash', Pair10.hash);
    console.log('Pair10 BlockNumber', txn2.blockNumber);
    console.log('Pair10 Logs', txn2.logs);
    console.log('Pair10 Events', txn2.events);
    var receipt = await hre.ethers.provider.getTransactionReceipt(Pair10.hash);
    console.log('receipts:', receipt);



    let Pair20 = await factory.createPairETH(
        nftaddress,//nft
        linearcurveaddress,//curve to be used
        '0x0000000000000000000000000000000000000000', //who is going to recieve the proceeds of sales
        2,//type 1 pool
        hre.ethers.utils.parseEther('0.0005'), //delta value (bignumber)
        hre.ethers.utils.parseEther('0.0000001'), //fee
        hre.ethers.utils.parseEther('0.000001'), //price of each NFT (spot price)
        [105, 106, 107],//NFT ids you want to transfer

        {
            value: ethers.utils.parseEther("0.001")//initail eth send to pool
        }
    )

    let txn3 = await Pair20.wait();
    console.log('Pool of type 2 created');
    console.log('Pair20 Hash', Pair20.hash);
    console.log('Pair20 BlockNumber', txn3.blockNumber);
    console.log('Pair20 Logs', txn3.logs);
    console.log('Pair20 Events', txn3.events);
    var receipt = await hre.ethers.provider.getTransactionReceipt(Pair20.hash);
    console.log('receipts', receipt);











}
main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
