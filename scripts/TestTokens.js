const { ethers } = require("hardhat");


addresstomint = '0x698c514c49C3E1C4285fc87674De84cd56A72646';
factoryaddr = ''
routeraddr = ''

async function main() {

    const Test721 = await ethers.getContractFactory("Test721");
    const test721 = await Test721.deploy();
    console.log("Nft address", test721.address);

    const initialMintCount = 50; // Number of NFTs to mint
    let initialMint = [];
    for (let i = 0; i <= initialMintCount; i++) {
        let tx = await test721.mint(addresstomint, i.toString());
        await tx.wait(); // wait for this tx to finish to avoid nonce issues
        initialMint.push(i.toString());
    }
    console.log(`${symbol} NFT with tokenIds ${initialMint} and minted to: ${addresstomint}\n`);


    const ETest721 = await ethers.getContractFactory("ERC721Enumerable");
    const etest721 = await ETest721.deploy();
    console.log("Enumerable Nft address", test721.address);

    const initialMintCounts = 50; // Number of NFTs to mint
    let initialMints = [];
    for (let i = 0; i <= initialMintCounts; i++) {
        let txs = await etest721.mint(addresstomint, i.toString());
        await txs.wait(); // wait for this tx to finish to avoid nonce issues
    }
    console.log('50 NFTS minted at address:', addresstomint);

    //approval for factory and router
    await test721.setApprovalForAll(factoryaddr, true);
    await test721.setApprovalForAll(routeraddr, true);
    await etest721.setApprovalForAll(factoryaddr, true);
    await etest721.setApprovalForAll(routeraddr, true);








}
main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});






